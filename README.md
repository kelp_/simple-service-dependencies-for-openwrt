# Simple Service Dependencies POC
This is a simple, copy-and-paste solution to get service dependencies in an openWRT init enviroment.

This solution hinges on procd's started service behaviour, in that you cannot have more than a single instance of a service running at one time. Put simply, there's not that much to it. 

Full dependency trees can be built by simply applying this same solution to subdependencies. 

SystemD target-like behaviour can also be achieved by creating a dummy service that simply has a lot of dependencies of it's own, and then using said dummy service as a dependency

# init.d example
```sh
#!/bin/sh /etc/rc.common

USE_PROCD=1

start_service() {
        # tell procd to start the dependancy, if it hasn't started already
        /etc/init.d/dependancy start
        
        # check if service has started successfully, log to syslog
        check=`ps | grep /usr/bin/dependancy_bin | wc -l`
        if [ "$check" != "2" ]; then
                logger -p err -t service_init "Failed to start example service due to missing service dependencies"
                return 1
        fi

        # we're good to go, continue standard service init   
        procd_open_instance
        procd_set_param command /usr/bin/example
        procd_set_param pidfile /var/run/example.pid
        procd_set_param file /etc/config/example
        procd_close_instance
}

service_triggers() {
        procd_add_reload_trigger "example"
}

reload_service() {
        stop
        start
}
```

# Package example

You can find two uncompiled packages in `./examples`. The two packages provided show how this mechanism can be applied when writing a pacakge set. 


