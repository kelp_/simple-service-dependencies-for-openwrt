#include <stdio.h>
#include <signal.h>

volatile sig_atomic_t deamonize = 1;

void term_proc(int sigterm) 
{
	deamonize = 0;
}

int main(void)
{
	FILE* fp;
	int i = 1;
	
	struct sigaction action;
	memset(&action, 0, sizeof(struct sigaction));
	action.sa_handler = term_proc;
	sigaction(SIGTERM, &action, NULL);

	while(deamonize) {
		fp = fopen("/tmp/example.txt", "a");
		fprintf(fp, "Main daemon heartbeat\n", i);
		fclose(fp);
		sleep(1);
	}
	if(fp != NULL) {
		fclose(fp);
	}
	return 0;
}
